django-axes (5.4.3-1) unstable; urgency=medium

  * New upstream version 5.4.3
  * d/control: Switch python3-django-ipware version to >= 3
  * d/control: Add build-dep on python3-setuptools-scm
  * d/rules: Set PYTHONPATH to . for test

 -- James Valleroy <jvalleroy@mailbox.org>  Sun, 09 Aug 2020 12:20:28 -0400

django-axes (5.0.7-2) unstable; urgency=medium

  * d/control: Use debhelper-compat = 13
  * d/control: Update standards version to 4.5.0
  * d/control: Add Rules-Requires-Root: no
  * Add d/upstream/metadata
  * d/control: Add versioned depend on python3-django-ipware << 3

 -- James Valleroy <jvalleroy@mailbox.org>  Wed, 29 Jul 2020 06:42:02 -0400

django-axes (5.0.7-1) unstable; urgency=low

  * New upstream version 5.0.7
  * d/control: Bump Standards-Version to 4.3.0
  * d/rules: CHANGES.txt moved to CHANGES.rst
  * debian: Use pytest for tests
  * d/control: Remove obsolete dependency python3-tz

 -- James Valleroy <jvalleroy@mailbox.org>  Sun, 07 Jul 2019 16:02:02 -0400

django-axes (4.4.0-1) unstable; urgency=medium

  * New upstream version 4.4.0
  * Bump Standards-Version to 4.2.1

 -- James Valleroy <jvalleroy@mailbox.org>  Sun, 30 Sep 2018 22:38:19 -0400

django-axes (4.3.1-1) unstable; urgency=low

  * d/copyright: Use https format uri
  * d/control: Update Vcs-* fields to salsa
  * d/control: Bump Standards-Version to 4.1.4
  * d/rules: Use dh_auto_test with custom args
  * d/compat: Switch to debhelper compat level 11
  * doc-base: HTML docs moved into main package folder
  * New upstream version 4.3.1 (Closes: #893699)

 -- James Valleroy <jvalleroy@mailbox.org>  Thu, 17 May 2018 06:03:04 -0400

django-axes (4.1.0-1) unstable; urgency=low

  * New upstream version 4.1.0.
  * Add build dependency on python3-django-ipware.
  * Bump standards version to 4.1.3.

 -- James Valleroy <jvalleroy@mailbox.org>  Sat, 24 Feb 2018 20:37:20 +0100

django-axes (3.0.3-1) unstable; urgency=medium

  * New upstream version 3.0.3. (Closes: #880833)
  * Add dependency on python3-django.

 -- James Valleroy <jvalleroy@mailbox.org>  Sat, 25 Nov 2017 14:50:33 -0500

django-axes (3.0.1-1) unstable; urgency=medium

  * New upstream version 3.0.1.
  * Remove patch for Django 1.11 support, merged upstream.
  * Add python3-django-appconf as build dependency.
  * Bump standards version to 4.1.1.
  * Use upstream tests for autopkgtest.
  * Add myself to uploaders.

 -- James Valleroy <jvalleroy@mailbox.org>  Sun, 19 Nov 2017 10:51:37 -0500

django-axes (2.3.3-1) unstable; urgency=low

  [ Joseph Nuthalapati ]
  * Initial package. (Closes: #874451)

  [ Sunil Mohan Adapa ]
  * Patch to fix automatic decoration of login view and tests.

 -- Federico Ceratto <federico@debian.org>  Sat, 30 Sep 2017 21:52:03 +0100
